import QtQuick 2.0
import QtQuick.Controls 2.12
import QtCharts 2.0
import QtQuick.Layouts 1.0
import nl.ysblokje.headtracker 1.0

Page {
    id: plotpage

    ColumnLayout {
        anchors.fill: parent

        ChartView {
            id: chartView
            title: "Line"
            anchors.fill: parent
            antialiasing: true

            ValueAxis {
                id: axisY
                min: 0
                max: 360
            }

            ValueAxis {
                id: axisX
                min: 0
                max: 256
            }

            LineSeries {
                id: tiltLineSeries
                name: "Tilt"
                axisX: axisX
                axisY: axisY
                //useOpenGL: chartView.openGL
            }

            LineSeries {
                id: rollLineSeries
                name: "Roll"
                axisX: axisX
                axisY: axisY
                //useOpenGL: chartView.openGL
            }

            LineSeries {
                id: panLineSeries
                name: "Pan"
                axisX: axisX
                axisY: axisY
                //useOpenGL: chartView.openGL
            }

            Timer {
                    id: refreshTimer
                    interval: 1 / 60 * 100 // 60 Hz
                    running: true
                    repeat: true
                    onTriggered: {
                        HeadTracker.update(chartView.series(0), 0);
                        HeadTracker.update(chartView.series(1), 1);
                        HeadTracker.update(chartView.series(2), 2);
                    }
                }

            function changeRefreshRate(rate) {
                refreshTimer.interval = 1 / Number(rate) * 1000;
            }
        }

        RowLayout {
            Layout.fillHeight: false
            Layout.fillWidth: false
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom

            Button {
                id: startplot
                text: qsTr("Start plot")
                onClicked: HeadTracker.streamTrackingData(true)
                enabled: HeadTracker.IsConnected
            }

            Button {
                id: clearplot
                text: qsTr("Clear plot")
                onClicked: {
                    //chartView.removeAllSeries();
                }
            }

            Button {
                id: stopplot
                text: qsTr("Stop plot")
                onClicked: HeadTracker.streamTrackingData(false)
                enabled: HeadTracker.IsConnected
            }
        }



    }

}



/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:1;anchors_x:0;anchors_y:0}
}
##^##*/
