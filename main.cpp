#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQuickControls2/QQuickStyle>
#include <QQuickItem>
#include <lib/connection.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    //QGuiApplication app(argc, argv);
    QApplication app(argc, argv);

    qmlRegisterSingletonType<HeadTracker>("nl.ysblokje.headtracker", 1, 0,
                                          "HeadTracker",
        [](QQmlEngine *engine, QJSEngine *scriptengine) -> QObject* {
            Q_UNUSED(engine)
            Q_UNUSED(scriptengine)
            auto ht = new HeadTracker();
            return ht;
        }
    );

    QQuickStyle::setStyle("Fusion");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);


    return app.exec();
}
