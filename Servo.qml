import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.0
import nl.ysblokje.headtracker 1.0


Page {
    id : servos

    GroupBox {
        id: groupBox
        anchors.fill: parent
        title: qsTr("Servo")

        RowLayout {
            anchors.fill: parent

            GroupBox {
                id: rowGroupBox
                y: 0
                width: 200
                height: 200
                visible: true
                Layout.fillHeight: true
                Layout.fillWidth: true
                font.underline: false
                title: qsTr("Roll")

                ColumnLayout {
                    id: rolllayout
                    anchors.fill: parent

                    Label {
                        id: label11
                        text: qsTr("Center")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: rollcenter
                        stepSize: 1
                        to: 400
                        Layout.fillWidth: true
                        Layout.preferredWidth: 109
                        value: HeadTracker.RollCenter
                        Layout.preferredHeight: 40
                    }

                    Label {
                        id: label12
                        text: qsTr("End ->")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: rollrightend
                        stepSize: 1
                        to: 1150
                        Layout.fillWidth: true
                        Layout.preferredWidth: 109
                        value: HeadTracker.RollMax
                        Layout.preferredHeight: 40
                        onValueChanged: HeadTracker.RollMax = value
                    }

                    Label {
                        id: label13
                        text: qsTr("<- End")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: rollleftend
                        snapMode: Slider.NoSnap
                        stepSize: 1
                        to: 1150
                        Layout.fillWidth: true
                        Layout.preferredWidth: 109
                        value: HeadTracker.RollMin
                        Layout.preferredHeight: 40
                        onValueChanged: HeadTracker.RollMin = value
                    }

                    Label {
                        id: label14
                        text: qsTr("Gain")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: rollgain
                        to: 500
                        stepSize: 1
                        Layout.fillWidth: true
                        Layout.preferredWidth: 109
                        value: HeadTracker.RollServoGain
                        Layout.preferredHeight: 40
                        onValueChanged: HeadTracker.RollServoGain = value
                    }

                    CheckBox {
                        id: rollreverse
                        text: qsTr("Reverse")
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: false
                    }
                }
            }

            GroupBox {
                id: tiltGroupBox
                width: 200
                height: 200
                Layout.fillHeight: true
                Layout.fillWidth: true
                font.underline: false
                title: qsTr("Tilt")

                ColumnLayout {
                    id: tiltlayout
                    anchors.fill: parent

                    Label {
                        id: label6
                        text: qsTr("Center")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: tiltcenter
                        stepSize: 1
                        to: 400
                        Layout.fillWidth: true
                        Layout.preferredWidth: 109
                        value: HeadTracker.TiltCenter;
                        Layout.preferredHeight: 40
                    }

                    Label {
                        id: label7
                        text: qsTr("End ->")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: tiltrightend
                        stepSize: 1
                        to: 1150
                        Layout.fillWidth: true
                        Layout.preferredWidth: 109
                        value: HeadTracker.TiltMax
                        Layout.preferredHeight: 40
                        onValueChanged: HeadTracker.TiltMax = value
                    }

                    Label {
                        id: label8
                        text: qsTr("<- End")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: tiltleftend
                        stepSize: 1
                        to: 1150
                        Layout.fillWidth: true
                        Layout.preferredWidth: 109
                        value: HeadTracker.TiltMin
                        Layout.preferredHeight: 40
                        onValueChanged: HeadTracker.TiltMin = value
                    }

                    Label {
                        id: label9
                        text: qsTr("Gain")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: tiltgain
                        stepSize: 1
                        to: 500
                        Layout.fillWidth: true
                        Layout.preferredWidth: 109
                        value: HeadTracker.TiltServoGain
                        onValueChanged: HeadTracker.TiltServoGain = value
                        Layout.preferredHeight: 40
                    }

                    CheckBox {
                        id: tiltreverse
                        text: qsTr("Reverse")
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: false
                    }
                }
            }

            GroupBox {
                id: panGroupBox
                Layout.fillWidth: true
                font.underline: false
                Layout.fillHeight: true
                title: qsTr("Pan")

                ColumnLayout {
                    id: panlayout
                    anchors.fill: parent

                    Label {
                        id: label1
                        text: qsTr("Center")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: pancenter
                        stepSize: 1
                        to: 400
                        Layout.fillWidth: true
                        Layout.preferredHeight: 40
                        Layout.preferredWidth: 109
                        value: HeadTracker.PanCenter;
                    }

                    Label {
                        id: label2
                        text: qsTr("End ->")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: panrightend
                        stepSize: 1
                        to: 1150
                        Layout.fillWidth: true
                        Layout.preferredHeight: 40
                        Layout.preferredWidth: 109
                        value: HeadTracker.PanMax;
                        onValueChanged: HeadTracker.PanMax = value
                    }

                    Label {
                        id: label3
                        text: qsTr("<- End")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: pandleftend
                        stepSize: 1
                        to: 1150
                        Layout.fillWidth: true
                        Layout.preferredHeight: 40
                        Layout.preferredWidth: 109
                        value: HeadTracker.PanMin;
                        onValueChanged: HeadTracker.PanMin = value
                    }

                    Label {
                        id: label4
                        text: qsTr("Gain")
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Slider {
                        id: pangain
                        stepSize: 1
                        to: 500
                        Layout.fillWidth: true
                        Layout.preferredHeight: 40
                        Layout.preferredWidth: 109
                        value: HeadTracker.PanServoGain
                        onValueChanged: HeadTracker.PanServoGain = value
                    }

                    CheckBox {
                        id: panreverse
                        text: qsTr("Reverse")

                        onStateChanged: HeadTracker.PanReverse = checked
                        checked : HeadTracker.PanReverse
                        //Connections {
                        //                        target: HeadTracker
                        //                        onStatusChanged: panreverse.checked = HeadTracker.PanReverse
                        //                    }

                        tristate: false
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: false
                    }
                }
            }
        }


    }


}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:4;anchors_x:0;anchors_y:0}D{i:15;anchors_x:0;anchors_y:0}
D{i:14;anchors_height:200;anchors_width:200}D{i:26;anchors_x:0;anchors_y:0}D{i:25;anchors_height:200;anchors_width:200}
D{i:2;anchors_x:6;anchors_y:6}D{i:1;anchors_height:200;anchors_width:200;anchors_x:46;anchors_y:42}
}
##^##*/
