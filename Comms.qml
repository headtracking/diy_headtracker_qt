import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.0
import QtQuick.Controls.Styles 1.4
import nl.ysblokje.headtracker 1.0


Page {
    id: commpage
    width: 800
    height: 195
    objectName: commpage

    Component.onCompleted: {
        HeadTracker.scanPorts()
    }


    GridLayout {
        id: commConfig
        x: 1
        y: 0
        Layout.fillHeight: false
        Layout.fillWidth: false
        rows: 2
        columns: 4

        ComboBox {
            id: commport
            width: 200
            Layout.fillWidth: true
            Layout.columnSpan: 2
            editable: true
            model: HeadTracker.SerialPorts

            onCurrentIndexChanged: {
                /* TODO : somebody entered something new */
                var _text = cbItems.get(currentIndex).text
                console.debug(_text)
                HeadTracker.DeviceName = _text
            }
        }

        Button {
            id: connect
            text: qsTr("&Connect")
            Layout.fillHeight: false
            Layout.fillWidth: false
            onClicked: HeadTracker.open()
            enabled: !HeadTracker.IsConnected
        }


        Button {
            id: upload
            text: qsTr("&Upload Settings")
            Layout.fillHeight: false
            Layout.fillWidth: true
            onClicked: HeadTracker.commitSettings()
            enabled: HeadTracker.IsConnected
        }


        ComboBox {
            id: baudrate
            width: 200
            Layout.fillWidth: true
            Layout.columnSpan: 2
            model: ListModel {
                id: cbbaudrate
                ListElement { text: "57600"; }
                ListElement { text: "115200"; }
                ListElement { text: "9600"; }
                ListElement { text: "2400"; }
            }
            editable: true
            onCurrentIndexChanged: {
                /* TODO : somebody entered something new */
                var _text = cbbaudrate.get(currentIndex).text
                console.debug(_text)
                HeadTracker.BaudRate = _text
            }
        }

        Button {
            id: disconnect
            text: qsTr("&Disconnect")
            Layout.fillHeight: false
            Layout.fillWidth: false
            onClicked: HeadTracker.close()
            enabled: HeadTracker.IsConnected
        }



        Button {
            id: calibrate
            text: qsTr("&Calibrate Gyro")
            Layout.fillHeight: false
            Layout.fillWidth: true
            enabled: HeadTracker.IsConnected
            //onClicked: HeadTracker.CalibrateGyro()
        }

        Label {
            id: label3
            text: qsTr("FW ver. : ")
        }

        TextField {
            id: fwField
            width: 70
            text: HeadTracker.FWVersion
            readOnly: true
        }

        Label {
            id: label4
            text: qsTr("HW ver. : ")
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }

        TextField {
            id: hwField
            width: 70
            text: HeadTracker.HWVersion
            readOnly: true
        }





    }

    ListView {
        id: listView
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.bottom: channelLayout.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: commConfig.right
        anchors.leftMargin: 6
        Layout.fillWidth: false
        Layout.fillHeight: true
        Layout.rowSpan: 3
        model: ListModel {
            ListElement {
                name: "Grey"
                colorCode: "grey"
            }

            ListElement {
                name: "Red"
                colorCode: "red"
            }

            ListElement {
                name: "Blue"
                colorCode: "blue"
            }

            ListElement {
                name: "Green"
                colorCode: "green"
            }
        }
        delegate: Item {
            x: 5
            width: 80
            height: 40
            Row {
                id: row1
                spacing: 10
                Rectangle {
                    width: 40
                    height: 40
                    color: colorCode
                }

                Text {
                    text: name
                    font.bold: true
                }
            }
        }
    }


    GridLayout {
        id: channelLayout
        height: 57
        anchors.left: parent.left
        anchors.leftMargin: 1
        anchors.right: commConfig.right
        anchors.rightMargin: 0
        anchors.top: commConfig.bottom
        anchors.topMargin: 5
        columns: 3

        ColumnLayout {

            Label {
                id: label2
                text: qsTr("Roll channel")
                Layout.preferredHeight: 16
                Layout.preferredWidth: 109
            }

            SpinBox {
                id: rollspinner
                Layout.preferredHeight: 24
                Layout.preferredWidth: 109
                onValueChanged: HeadTracker.RollChannel = value
                value: HeadTracker.RollChannel
            }
        }

        ColumnLayout {

            Label {
                id: label1
                text: qsTr("Tilt channel")
                Layout.preferredHeight: 16
                Layout.preferredWidth: 109
            }

            SpinBox {
                id: tiltspinner
                Layout.preferredHeight: 24
                Layout.preferredWidth: 109
                onValueChanged: HeadTracker.TiltChannel = value
                value: HeadTracker.TiltChannel
            }
        }

        ColumnLayout {

            Label {
                id: label
                text: qsTr("Pan channel")
                Layout.preferredHeight: 16
                Layout.preferredWidth: 109
            }

            SpinBox {
                id: panspinner
                Layout.preferredHeight: 24
                Layout.preferredWidth: 109
                onValueChanged: HeadTracker.PanChannel = value
                value: HeadTracker.PanChannel
            }
        }
    }

}





/*##^##
Designer {
    D{i:16;anchors_x:0}D{i:1;anchors_x:0;anchors_y:0}D{i:26;anchors_height:192;anchors_width:224;anchors_x:576;anchors_y:0}
D{i:36;anchors_height:57;anchors_width:569;anchors_x:0;anchors_y:0}
}
##^##*/
