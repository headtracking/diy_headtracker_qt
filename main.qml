import QtQuick 2.13
import QtQuick.Controls 2.12
import QtQuick.Window 2.13
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2

ApplicationWindow {
    id: applicationWindow
    visible: true
    title: "DIY Headtracker GUI Qt"
    width: 600
    height: 900
    color: "#158df6"

    menuBar: MenuBar {
        Menu {
            title: "File"
        }
        Menu {
            title: "Show"
        }
        Menu {
            title: "Help"
        }
    }

    Servo {
        id: servo
        y: 0
        height: 392
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: comms.bottom
        anchors.topMargin: 0
    }

    Comms {
        id: comms
        height: 191
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 0
    }



    PlotPage {
        id: plotPage
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 6
        anchors.top: servo.bottom
        anchors.topMargin: 0
    }

    FileDialog {
        id: fileDialog
        title: "Choose configuration file"
        folder: shortcuts.home
    }


    FileDialog {
        id: fileSaveDialog
        title: "Please select filename to save"
        folder: shortcuts.home
        selectExisting: false
    }

}



/*##^##
Designer {
    D{i:5;anchors_height:600;anchors_width:500;anchors_x:5;anchors_y:139}D{i:6;anchors_height:600;anchors_width:500;anchors_x:39;anchors_y:0}
D{i:8;anchors_x:121;anchors_y:139}D{i:9;anchors_height:600;anchors_width:500;anchors_x:39;anchors_y:0}
}
##^##*/
