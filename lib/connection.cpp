#include "connection.hpp"
#include <iostream>
#include <QVariant>
#include <QtCharts/QXYSeries>



HeadTracker::HeadTracker(QObject* parent) : QObject(parent), port(parent) {
    connect(&port, &QSerialPort::readyRead, this, &HeadTracker::handleReadyRead);
    setProperty("FWVersion", "N/A");
    setProperty("HWVersion", "N/A");
    m_data.push_back(QVector<QPointF>());
    m_data.push_back(QVector<QPointF>());
    m_data.push_back(QVector<QPointF>());
}

HeadTracker::~HeadTracker() {
    std::cout << devicename.toUtf8().data() << "\n";
}

void HeadTracker::scanPorts() {
    auto ports = QSerialPortInfo::availablePorts();
    for(auto const &p : ports) {
        _serialports.push_back(p.portName());
    }
    SerialPortsChanged();
}

const QStringList HeadTracker::getSerialPorts() {
    return _serialports;
}


void HeadTracker::open() {
    close();
    port.setPortName(devicename);
    port.setBaudRate(baudrate);
    port.setDataBits(QSerialPort::Data8);
    port.setParity(QSerialPort::NoParity);
    port.setStopBits(QSerialPort::OneStop);

    _IncomingData.clear();
    response_line.clear();

    if(port.open(QIODevice::ReadWrite)) {
        GetFWVersion();
        GetHWVersion();
        RetrieveSettings();
    }
    setProperty("IsConnected", "true");
}

void HeadTracker::close() {
    port.close();
    setProperty("IsConnected", "false");
}


void HeadTracker::parseData() {
    if(_IncomingData.isEmpty())
        return;

    for(int index{}; index < _IncomingData.size(); index++) {
        auto character = _IncomingData.at(index);
        bool line_ready{false};
        switch(character) {
            case '\n':
                line_ready = true;
            break;
            case '\r':
                continue;
            default:
                response_line.append(character);
        }
        if(line_ready) {
            parseResponse();
            appendLog(response_line);
            response_line.clear();
        }
    }
    _IncomingData.clear();
}

void HeadTracker::parseResponse() {
    if(response_line.length() > 4 && response_line.left(4) == "FW: ") {
        setProperty("FWVersion", response_line.mid(4));
        return;
    }

    if(response_line.length() > 4 && response_line.left(4) == "HW: ") {
        setProperty("HWVersion", response_line.mid(4));
        return;
    }

    if(response_line.length() > 5 && response_line.left(5).compare("$SET$") == 0) {
        parseSettings();
        return;
    }
    parseStream();
}


void HeadTracker::parseStream() {
    auto items = response_line.split(',');
    if(items.count() < 3)
        return;

    m_data[0].push_back(QPointF(m_index, items[0].toFloat()));
    m_data[1].push_back(QPointF(m_index, items[1].toFloat()));
    m_data[2].push_back(QPointF(m_index, items[2].toFloat()));
    ++m_index;
    if(m_data[0].count()>256) {
        m_data[0].pop_front();
    }
    if(m_data[1].count()>256) {
        m_data[1].pop_front();
    }
    if(m_data[1].count()>256) {
        m_data[1].pop_front();
    }
}



void HeadTracker::parseSettings() {
    HTSETTINGS ht{};


    QString toParse = response_line.mid(5);
    auto items = toParse.split(',');
    if ( HT_SETTINGS_COUNT <= items.length())
    {
        setProperty("LPTiltRoll", items[0].toInt());
        setProperty("LPPan", items[1].toInt());

        setProperty("GyroWeightTiltRoll", items[2].toInt());
        setProperty("GyroWeightPan", items[3].toInt());
        setProperty("TiltServoGain", items[4].toInt());
        setProperty("PanServoGain", items[5].toInt());
        setProperty("RollServoGain", items[6].toInt());
        auto ServoReverse = items[7].toUInt();

        auto reversetilt = (ServoReverse & HT_TILT_REVERSE_BIT) > 0;
        auto reverseroll = (ServoReverse & HT_ROLL_REVERSE_BIT) > 0;
        auto reversepan = (ServoReverse & HT_PAN_REVERSE_BIT) > 0;

        setProperty("RollReverse", reverseroll);
        setProperty("TiltReverse", reversetilt);
        setProperty("PanReverse", reversepan);

        setProperty("PanCenter", items[8].toInt());
        setProperty("PanMin", items[9].toInt());
        setProperty("PanMax", items[10].toInt());
        setProperty("TiltCenter", items[11].toInt());
        setProperty("TiltMin", items[12].toInt());
        setProperty("TiltMax", items[13].toInt());
        setProperty("RollCenter", items[14].toInt());
        setProperty("RollMin", items[15].toInt());
        setProperty("RollMax", items[16].toInt());
        setProperty("PanChannel", items[17]);
        setProperty("TiltChannel", items[18]);
        setProperty("RollChannel", items[19]);
    }
}

void HeadTracker::RetrieveSettings() {
    executeCommand("GSET");
}

void HeadTracker::executeCommand(QString cmd) {
    if(port.isOpen()) {
        cmd.prepend('$');
        cmd.append('\n');
        auto data = cmd.toUtf8().data();
        port.write(data);
    }
}

void HeadTracker::handleReadyRead() {
    _IncomingData.append(port.readAll());
    parseData();
}

void HeadTracker::CommitSettings() {


}

void HeadTracker::GetFWVersion() {
    executeCommand("VERS");
}

void HeadTracker::GetHWVersion() {
    executeCommand("HARD");
}


void HeadTracker::streamTrackingData(bool start) {
    if(start) {
        executeCommand("PLST");
    } else {
        executeCommand("PLEN");
        _AccelStreaming = false;
        _MagStreaming = false;
        _MagAccelStreaming = false;
    }
    _TrackStreaming = true;
    _Streaming = true;
}
void HeadTracker::StreamAccelData(bool start) {
    if(start) {
        executeCommand("GRAV");
    } else {
        executeCommand("GREN");
    }
}
void HeadTracker::StreamMagData(bool start) {
    if(start) {
        executeCommand("CAST");
    } else {
        executeCommand("CAEN");
    }
}
void HeadTracker::StringMagAccelData(bool start) {
    if(start) {
        executeCommand("CMAS");
    } else {
        executeCommand("CMAE");
    }
}
void HeadTracker::StoreAccelCal(int XOffset, int YOffset, int ZOffset) {
    auto xtemp = XOffset + 2000;
    auto ytemp = YOffset + 2000;
    auto ztemp = ZOffset + 2000;
    executeCommand(QString("%1,%2,%3ACC").arg(xtemp, ytemp, ztemp));
}

void HeadTracker::ResetAccelCal() {
    StoreAccelCal(0,0,0);
}

void HeadTracker::StoreMagCal(int XOffset, int YOffset, int ZOffset) {
    auto xtemp = XOffset + 2000;
    auto ytemp = YOffset + 2000;
    auto ztemp = ZOffset + 2000;
    executeCommand(QString("%1,%2,%3MAG").arg(xtemp, ytemp, ztemp));
}
void HeadTracker::ResetMagCal() {
    StoreMagCal(0,0,0);
}
void HeadTracker::calibrateGyro() {
    executeCommand("CALG");
}


void HeadTracker::update(QtCharts::QAbstractSeries *series, int channel)
{
    if (series) {
        auto xySeries = static_cast<QtCharts::QXYSeries *>(series);
        if(channel > m_data.count())
            channel = 0;

        QVector<QPointF> points = m_data.at(channel);
        // Use replace instead of clear + append, it's optimized for performance
        xySeries->replace(points);
    }
}


void HeadTracker::appendLog(const QString &line) {
    _logging_data.append(line);
    LoggingChanged();
}
