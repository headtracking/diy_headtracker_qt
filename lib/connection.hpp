#include <QtCore/QObject>
#include <QtCore/QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QStringList>
#include <QList>
#include <QtCharts/QAbstractSeries>
#include <cstdint>

#define HT_TILT_REVERSE_BIT     0x01
#define HT_ROLL_REVERSE_BIT     0x02
#define HT_PAN_REVERSE_BIT      0x04
#define HT_SETTINGS_COUNT       21


// Settings
//
struct HTSETTINGS
{
    int LPTiltRoll;         // Firmware: tiltRollBeta
    int LPPan;              // Firmware: panBeta
    int GyroWeightTiltRoll; // Firmware: gyroWeightTiltRoll
    int GyroWeightPan;      // Firmware: GyroWeightPan
    int ServoGainPan;       // Firmware: tiltFactor
    int ServoGainTilt;      // Firmware: panFactor
    int ServoGainRoll;      // Firmware: rollFactor
    uint8_t ServoReverse;   // Firmware: servoReverseMask
    int PanCenter;          // Firmware: servoPanCenter
    int PanMin;             // Firmware: panMinPulse
    int PanMax;             // Firmware: panMaxPulse
    int TiltCenter;         // Firmware: servoTiltCenter
    int TiltMin;            // Firmware: tiltMinPulse
    int TiltMax;            // Firmware: tiltMaxPulse
    int RollCenter;         // Firmware: servoRollCenter
    int RollMin;            // Firmware: rollMinPulse
    int RollMax;            // Firmware: rollMaxPulse
    uint8_t PanCh;          // Firmware: htChannels[0]
    uint8_t TiltCh;         // Firmware: htChannels[1]
    uint8_t RollCh;         // Firmware: htChannels[2]
    int bt_mode;
};



class HeadTracker : public QObject {
    Q_OBJECT

    Q_PROPERTY(bool AccelStreaming MEMBER _AccelStreaming)
    Q_PROPERTY(bool MagStreaming MEMBER _MagStreaming)
    Q_PROPERTY(bool MagAccelStreaming MEMBER _MagAccelStreaming)
    Q_PROPERTY(bool TrackStreaming MEMBER _TrackStreaming)
    Q_PROPERTY(bool Streaming MEMBER _Streaming)
    Q_PROPERTY(QString FWVersion MEMBER _FWVersion NOTIFY FWVersionChanged)
    Q_PROPERTY(QString HWVersion MEMBER _HWVersion NOTIFY HWVersionChanged)
    Q_PROPERTY(qint32 BaudRate MEMBER baudrate)
    Q_PROPERTY(QString DeviceName MEMBER devicename)
    Q_PROPERTY(int LPTiltRoll MEMBER LPTiltRoll NOTIFY LPTiltRollChanged)
    Q_PROPERTY(int LPPan MEMBER LPPan NOTIFY LPPanChanged)

    Q_PROPERTY(int PanCenter MEMBER PanCenter NOTIFY PanCenterChanged)
    Q_PROPERTY(int PanMin MEMBER PanMin NOTIFY PanMinChanged)
    Q_PROPERTY(int PanMax MEMBER PanMax NOTIFY PanMaxChanged)
    Q_PROPERTY(int PanChannel MEMBER PanChannel NOTIFY PanChannelChanged)
    Q_PROPERTY(bool PanReverse MEMBER PanReverse NOTIFY PanReverseChanged)
    Q_PROPERTY(int PanServoGain MEMBER PanServoGain NOTIFY PanServoGainChanged)
    Q_PROPERTY(int PanGyroWeight MEMBER PanGyroWeight NOTIFY PanGyroWeightChanged)

    Q_PROPERTY(int TiltCenter MEMBER TiltCenter NOTIFY TiltCenterChanged)
    Q_PROPERTY(int TiltMin MEMBER TiltMin NOTIFY TiltMinChanged)
    Q_PROPERTY(int TiltMax MEMBER TiltMax NOTIFY TiltMaxChanged)
    Q_PROPERTY(int TiltChannel MEMBER TiltChannel NOTIFY TiltChannelChanged)
    Q_PROPERTY(bool TiltReverse MEMBER TiltReverse NOTIFY TiltReverseChanged)
    Q_PROPERTY(int TiltServoGain MEMBER TiltServoGain NOTIFY TiltServoGainChanged)
    Q_PROPERTY(int TiltGyroWeight MEMBER TiltGyroWeight NOTIFY TiltGyroWeightChanged)

    Q_PROPERTY(int RollCenter MEMBER RollCenter NOTIFY RollCenterChanged)
    Q_PROPERTY(int RollMin MEMBER RollMin NOTIFY RollMinChanged)
    Q_PROPERTY(int RollMax MEMBER RollMax NOTIFY RollMaxChanged)
    Q_PROPERTY(int RollChannel MEMBER RollChannel NOTIFY RollChannelChanged)
    Q_PROPERTY(bool RollReverse MEMBER RollReverse NOTIFY RollReverseChanged)
    Q_PROPERTY(int RollServoGain MEMBER RollServoGain NOTIFY RollServoGainChanged)
    Q_PROPERTY(int RollGyroWeight MEMBER RollGyroWeight NOTIFY RollGyroWeightChanged)
    Q_PROPERTY(bool IsConnected MEMBER connected NOTIFY is_connected)
    Q_PROPERTY(QStringList LoggingData MEMBER _logging_data NOTIFY LoggingChanged)
    Q_PROPERTY(QStringList SerialPorts READ getSerialPorts NOTIFY SerialPortsChanged)
public:
    explicit HeadTracker(QObject* parent=nullptr);
    ~HeadTracker();

public slots:
    void open();
    void close();
    void RetrieveSettings();
    void CommitSettings();
    void GetFWVersion();
    void GetHWVersion();
    void streamTrackingData(bool start);
    void StreamAccelData(bool start);
    void StreamMagData(bool start);
    void StringMagAccelData(bool start);
    void StoreAccelCal(int XOffset, int YOffset, int ZOffset);
    void ResetAccelCal();
    void StoreMagCal(int XOffset, int YOffset, int ZOffset);
    void ResetMagCal();
    void calibrateGyro();

    void update(QtCharts::QAbstractSeries *series, int channel);

    void scanPorts();
    const QStringList getSerialPorts();
private slots:
    /* connected to the port */
    void handleReadyRead();

private:
    void executeCommand(QString cmd);
    void parseData();
    void parseResponse();
    void parseSettings();
    void parseStream();
    void appendLog(const QString &line);


signals:
    void SerialPortsChanged();
    void is_connected();
    void LoggingChanged();
    void FWVersionChanged();
    void HWVersionChanged();
    void LPTiltRollChanged();
    void LPPanChanged();

    void PanCenterChanged();
    void PanMinChanged();
    void PanMaxChanged();
    void PanChannelChanged();
    void PanReverseChanged();
    void PanServoGainChanged();
    void PanGyroWeightChanged();

    void TiltCenterChanged();
    void TiltMinChanged();
    void TiltMaxChanged();
    void TiltChannelChanged();
    void TiltReverseChanged();
    void TiltServoGainChanged();
    void TiltGyroWeightChanged();

    void RollCenterChanged();
    void RollMinChanged();
    void RollMaxChanged();
    void RollChannelChanged();
    void RollReverseChanged();
    void RollServoGainChanged();
    void RollGyroWeightChanged();

private:
    QStringList _serialports;
    QVector<QVector<QPointF>> m_data;
    //_tilt_data;
    QVector<QPointF> _roll_data;
    QVector<QPointF> _pan_data;
    int m_index{};
    QByteArray _IncomingData;
    QString response_line;
    QSerialPort port;
    qint32 baudrate = 57600;
    QString devicename = "/dev/ttyACM0";
    bool _TrackStreaming = false;
    bool _MagStreaming = false;
    bool _AccelStreaming = false;
    bool _MagAccelStreaming = false;
    bool _Streaming = false;
    bool connected{false};
    QString _FWVersion = "N/A";
    QString _HWVersion = "N/A";
    QStringList _logging_data;


    int LPTiltRoll{};
    int LPPan{};

    int PanCenter{};
    int PanMin{};
    int PanMax{};
    int PanChannel{};
    bool PanReverse{};
    int PanServoGain{};
    int PanGyroWeight{};

    int TiltCenter{};
    int TiltMin{};
    int TiltMax{};
    int TiltChannel{};
    bool TiltReverse{};
    int TiltServoGain{};
    int TiltGyroWeight{};

    int RollCenter{};
    int RollMin{};
    int RollMax{};
    int RollChannel{};
    bool RollReverse{};
    int RollServoGain{};
    int RollGyroWeight{};
};
